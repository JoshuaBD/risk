﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    [Table("Jugadors")]
    public class Jugador
    {
        public int JugadorID { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required, Column("ordre_tirada")]
        public int OrdreTirada { get; set; }
        [Required, Column("quantitat_regions")]
        public int QuantitatRegions { get; set; }
        [Required, Column("num_victories")]
        public int NumVictories { get; set; }

        //Relació n a 1 amb Region
        public virtual ICollection<Region> Regions { get; set; }

        //Relació n a 1 amb Continent
        public virtual ICollection<Continent> Continents { get; set; }

        public Jugador()
        {
            this.Regions = new HashSet<Region>();
            this.Continents = new HashSet<Continent>();
        }

    }
}
