﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    class Services
    {
        public static RiskContext ctx = new RiskContext();

        public static void attack(int ordre)
        {
            Console.WriteLine();
            Console.WriteLine("================= FASE D'ATAC =================");
            Console.WriteLine();

            Jugador jugador = ctx.Jugador.Where(j => j.OrdreTirada == ordre).FirstOrDefault();
            bool cont = true;
            do
            {
                List<Region> regions = ctx.Region.ToList()
                                    .Where(j => j.Jugador == jugador)
                                    .OrderBy(o => o.RegionID).ToList();

                bool potAtacar = false;

                foreach(Region r in regions)
                {
                    foreach (Region rv in r.Regions2)
                    {
                        if (r.Tropes > 1 && rv.Jugador != jugador)
                        {
                            potAtacar = true;
                            break;
                        }
                    }

                    if (potAtacar) break;
                }

                if (cont && potAtacar)
                {
                    char continuar = ' ';
                    Console.WriteLine("Jugador " + jugador.Nom + "! Vols atacar? (S/N)");
                    do
                    {
                        continuar = Char.ToLower(Console.ReadLine()[0]); //Primer char de un String
                        if (continuar != 'n' && continuar != 's')
                        {
                            Console.WriteLine("Siusplau, contesta S/N!");
                        }

                    } while (continuar != 'n' && continuar != 's');

                    if (continuar == 's')
                    {
                        Console.WriteLine();

                        regions = ctx.Region.ToList()
                                    .Where(j => j.Jugador == jugador)
                                    .OrderBy(o => o.RegionID).ToList();

                        List<int> regionsId = new List<int>();

                        Console.WriteLine("Regions disponibles: ");
                        foreach (Region r in regions)
                        {
                            String regionsEnemigues = "";

                            foreach (Region rv in r.Regions2)
                            {
                                if (rv.Jugador != jugador)
                                {
                                    regionsEnemigues += rv.RegionID + "(" + rv.Tropes + ") ";

                                    if (r.Tropes > 1) regionsId.Add(r.RegionID);
                                }
                            }

                            if (regionsEnemigues.Equals("")) { regionsEnemigues = "Cap regió enemiga"; }

                            Console.WriteLine("ID: " + r.RegionID + " | Nom: " + r.Nom + " | Tropes: " + r.Tropes + " | RegionsEnemigues(Tropes): " + regionsEnemigues);
                        }

                        Console.WriteLine("Jugador " + jugador.Nom + "! Des de quina regió vols atacar? (Introdueix la ID) \n(Només pots atacar desde una regió que tingui més de 1 Tropes i que tingui regions enemigues veines!)");
                        int rResposta = 0;

                        do
                        {
                            try
                            {
                                rResposta = Convert.ToInt32(Console.ReadLine());
                                if (!regionsId.Contains(rResposta))
                                {
                                    Console.WriteLine("Siusplau, digues una ID de les regions que tens i que sigui vàlid per a atacar!");
                                }
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("NOMÉS NUMEROS!!!");
                            }
                        } while (!regionsId.Contains(rResposta));

                        Console.WriteLine();

                        regionsId = new List<int>();
                        Region regioSelected = null;

                        foreach (Region r in regions)
                        {
                            if (r.RegionID == rResposta)
                            {
                                regioSelected = r;
                                break;
                            }
                        }

                        List<Region> regionsEne = regioSelected.Regions2.ToList();
                        foreach (Region rv in regionsEne)
                        {
                            if (rv.Jugador != jugador)
                            {
                                Console.WriteLine("ID: " + rv.RegionID + " | Nom: " + rv.Nom + " | Tropes: " + rv.Tropes);
                                regionsId.Add(rv.RegionID);
                            }
                        }

                        Console.WriteLine("Ara a quina regió enemiga veina vols atacar? (Introdueix la ID)");

                        int reResposta = 0;
                        do
                        {
                            try
                            {
                                reResposta = Convert.ToInt32(Console.ReadLine());
                                if (!regionsId.Contains(reResposta))
                                {
                                    Console.WriteLine("Siusplau, digues una ID de les regions enemigues!");
                                }
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine("NOMÉS NUMEROS!!!");
                            }
                        } while (!regionsId.Contains(reResposta));

                        Region regioSelectedDef = null;
                        foreach (Region r in regioSelected.Regions2)
                        {
                            if (r.RegionID == reResposta)
                            {
                                regioSelectedDef = r;
                                break;
                            }
                        }

                        int tropesAtacar = regioSelected.Tropes - 1;
                        int tropesDef = regioSelectedDef.Tropes;

                        if (tropesAtacar > 3) tropesAtacar = 3;
                        if (tropesDef > 2) tropesDef = 2;

                        Tirada.DiceRoll(tropesAtacar, tropesDef, out int tropesAtacRest, out int tropesDefRest);

                        regioSelected.Tropes -= tropesAtacRest;
                        regioSelectedDef.Tropes -= tropesDefRest;

                        Console.WriteLine();

                        Console.WriteLine("Resultat de la batalla: " +
                            "\n\t Tropes restants a la regió aliada " + regioSelected.Nom + ": " + regioSelected.Tropes + " | Tropes perdudes: " + tropesAtacRest +
                            "\n\t Tropes restants a la regió enemiga " + regioSelectedDef.Nom + ": " + regioSelectedDef.Tropes + " | Tropes perdudes: " + tropesDefRest);

                        Console.WriteLine();

                        if (regioSelectedDef.Tropes <= 0)
                        {
                            conquista(regioSelectedDef, jugador, regioSelected.Tropes);
                            regioSelected.Tropes = 1;

                            Console.WriteLine("Has conquistat la regió " + regioSelectedDef.Nom);

                            check(ordre); //MIREM A VEURE SI S'HA ACABAT LA PARTIDA, HO FAIG AQUÍ PERQUE AIXÍ IMPEDIM QUE NO ES PUGUI ATACAR INFINITAMENT
                        }

                        ctx.SaveChanges();

                    }
                    else cont = false;
                }
                else
                {
                    cont = false;
                    Console.WriteLine("Ja no pots atacar, cap de les teves regions té tropes per atacar...");
                }
            } while (cont);

            ctx.SaveChanges();
        }

        public static void reorganizar(int ordre)
        {
            Console.WriteLine();
            Console.WriteLine("================= FASE DE REORGANITZAR =================");
            Console.WriteLine();

            Jugador jugador = ctx.Jugador.Where(j => j.OrdreTirada == ordre).FirstOrDefault();

            List<Region> regions = ctx.Region
                                .Where(j => j.Jugador.JugadorID == jugador.JugadorID)
                                .OrderBy(o => o.RegionID).ToList();

            bool potReorganitzar = false;

            foreach (Region r in regions)
            {
                foreach (Region rv in r.Regions2)
                {
                    if (r.Tropes > 1 && rv.Jugador == jugador)
                    {
                        potReorganitzar = true;
                        break;
                    }
                }

                if (potReorganitzar) break;
            }

            if (potReorganitzar)
            {
                char continuar = ' ';
                Console.WriteLine("Jugador " + jugador.Nom + "! Vols reorganitzar tropes? (S/N)");
                do
                {
                    continuar = Char.ToLower(Console.ReadLine()[0]); //Primer char de un String
                    if (continuar != 'n' && continuar != 's')
                    {
                        Console.WriteLine("Siusplau, contesta S/N!");
                    }

                } while (continuar != 'n' && continuar != 's');

                if (continuar == 's')
                {
                    Console.WriteLine();

                    regions = ctx.Region
                                .Where(j => j.Jugador.JugadorID == jugador.JugadorID)
                                .OrderBy(o => o.RegionID).ToList();

                    List<int> regionsId = new List<int>();

                    Console.WriteLine("Regions disponibles: ");
                    foreach (Region r in regions)
                    {
                        String regionsAl = "";

                        foreach (Region rv in r.Regions2)
                        {
                            if (rv.Jugador == jugador)
                            {
                                regionsAl += rv.RegionID + "(" + rv.Tropes + ") ";

                                if (r.Tropes > 1) regionsId.Add(r.RegionID);
                            }
                        }

                        if (regionsAl.Equals("")) { regionsAl = "Cap regió aliada"; }

                        Console.WriteLine("ID: " + r.RegionID + " | Nom: " + r.Nom + " | Tropes: " + r.Tropes + " | RegionsAliades(Tropes): " + regionsAl);
                    }

                    Console.WriteLine("Jugador " + jugador.Nom + "! Des de quina regió vols reorganitzar? (Introdueix la ID) \n(Només pots reorganitzar desde una regió que tingui més de 1 Tropes i que tingui regions aliades veines!)");
                    int rResposta = 0;

                    do
                    {
                        try
                        {
                            rResposta = Convert.ToInt32(Console.ReadLine());
                            if (!regionsId.Contains(rResposta))
                            {
                                Console.WriteLine("Siusplau, digues una ID de les regions que tens i que sigui vàlid per a reorganitzar!");
                            }
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("NOMÉS NUMEROS!!!");
                        }
                    } while (!regionsId.Contains(rResposta));

                    Console.WriteLine();

                    regionsId = new List<int>();
                    Region regioSelected = null;

                    foreach (Region r in regions)
                    {
                        if (r.RegionID == rResposta)
                        {
                            regioSelected = r;
                            break;
                        }
                    }

                    List<Region> regionsAliades = regioSelected.Regions2.ToList();
                    Console.WriteLine("Regions Disponibles: ");
                    foreach (Region rv in regionsAliades)
                    {
                        if (rv.Jugador == jugador)
                        {
                            Console.WriteLine("ID: " + rv.RegionID + " | Nom: " + rv.Nom + " | Tropes: " + rv.Tropes);
                            regionsId.Add(rv.RegionID);
                        }
                    }

                    Console.WriteLine("Ara a quina regió aliada veina vols posar tropes? (Introdueix la ID)");

                    int reResposta = 0;
                    do
                    {
                        try
                        {
                            reResposta = Convert.ToInt32(Console.ReadLine());
                            if (!regionsId.Contains(reResposta))
                            {
                                Console.WriteLine("Siusplau, digues una ID de les regions enemigues!");
                            }
                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("NOMÉS NUMEROS!!!");
                        }
                    } while (!regionsId.Contains(reResposta));

                    Region regioSelectedMoure = null;
                    foreach (Region r in regioSelected.Regions2)
                    {
                        if (r.RegionID == reResposta)
                        {
                            regioSelectedMoure = r;
                            break;
                        }
                    }

                    Console.WriteLine();
                    Console.WriteLine("Quantes tropes hi vols posar? ");

                    int tResposta = 0;
                    do
                    {
                        try
                        {
                            tResposta = Convert.ToInt32(Console.ReadLine());
                            if (tResposta <= 0 || tResposta > regioSelected.Tropes - 1)
                            {
                                Console.WriteLine("Siusplau, digues un numero superior a 1 i menor o igual que " + (regioSelected.Tropes - 1));
                            }

                        }
                        catch (FormatException e)
                        {
                            Console.WriteLine("NOMÉS NUMEROS!!!");
                        }
                    } while (tResposta <= 0 || tResposta > regioSelected.Tropes - 1);

                    regioSelected.Tropes -= tResposta;
                    regioSelectedMoure.Tropes += tResposta;
                }
            }

            ctx.SaveChanges();
        }

        public static void conquista(Region regio, Jugador jugador, int tropes)
        {
            regio.Jugador = jugador;
            regio.Tropes = tropes - 1;

            jugador.QuantitatRegions += 1;
            Jugador rival = ctx.Jugador.Where(j => j.JugadorID != jugador.JugadorID).FirstOrDefault();
            rival.QuantitatRegions -= 1;

            int cont1 = 0;
            int cont2 = 0;

            foreach(Region r in ctx.Region.ToArray()) //Comprovació canvi en els continents...
            {
                if (r.Continent.ContinentID == 1 && r.Jugador == jugador) cont1++;
                if (r.Continent.ContinentID == 2 && r.Jugador == jugador) cont2++;
            }

            if (cont1 == 4) ctx.Continent.Where(c => c.ContinentID == 1).FirstOrDefault().Jugador = jugador;
            else if(cont1 > 0) ctx.Continent.Where(c => c.ContinentID == 1).FirstOrDefault().Jugador = null;

            if (cont2 == 4) ctx.Continent.Where(c => c.ContinentID == 2).FirstOrDefault().Jugador = jugador;
            else if(cont2 > 0) ctx.Continent.Where(c => c.ContinentID == 2).FirstOrDefault().Jugador = null;

        }

        public static void check(int ordre)
        {
            Jugador jugador = ctx.Jugador.Where(j => j.OrdreTirada == ordre).FirstOrDefault();

            bool acabar = false;

            if (jugador.QuantitatRegions == 8) acabar = true;

            if (acabar)
            {
                Console.WriteLine("FELICITATS JUGADOR " + jugador.Nom + " HAS CONQUISTAT TOTES LES REGIONS! ETS EL GUANYADOR! (Qualsevol tecla per sortir...)");
                Console.ReadLine();
                Environment.Exit(0); //A lo loco, pero funciona. Un poco como lo que hiciste tu con mi php del XAMPP
            }
        }

        public static void turnBeggining(int ordre)
        {

            Jugador jugador = ctx.Jugador.Where(j => j.OrdreTirada == ordre).FirstOrDefault();
            Console.WriteLine();
            Console.WriteLine("Es el torn del Jugador " + jugador.Nom);
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("================= PRINCIPI TORN =================");
            Console.WriteLine();

            List<Continent> continents;
            List<Region> regions;

            continents = ctx.Continent.ToList();
            regions = ctx.Region.ToList();

            int tropas = 5 + (jugador.QuantitatRegions / 3);

            foreach(Continent c in continents)
            {
                if(c.Jugador == jugador) tropas += c.BonusTropes;
            }

            List<Region> regionsJugador = new List<Region>();

            List<int> regionsId = new List<int>();
            foreach (Region r in regions)
            {
                if (r.Jugador == jugador)
                {
                    regionsJugador.Add(r);
                    regionsId.Add(r.RegionID);
                }
            }
            List<Region> SortedList = regionsJugador.OrderBy(o => o.RegionID).ToList();

            do {
                Console.WriteLine("Regions disponibles: ");
                foreach (Region r in SortedList) { Console.WriteLine("ID: " + r.RegionID + " | Nom: " + r.Nom + " | Tropes: " + r.Tropes); }
                Console.WriteLine("Jugador " + jugador.Nom + ", tens " + tropas + " tropes per col·locar. A quina regió vols col·locar tropes? (Introdueix la ID)");

                int rResposta = 0;
                do
                {
                    try
                    {
                        rResposta = Convert.ToInt32(Console.ReadLine());
                        if (!regionsId.Contains(rResposta))
                        {
                            Console.WriteLine("Siusplau, digues una ID de les regions que tens!");
                        }
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("NOMÉS NUMEROS!!!");
                    }
                } while (!regionsId.Contains(rResposta));

                Console.WriteLine();

                Console.WriteLine("Quantes tropes hi vols posar? ");

                int tResposta = 0;
                do
                {
                    try
                    {
                        tResposta = Convert.ToInt32(Console.ReadLine());
                        if (tResposta <= 0 || tResposta > tropas)
                        {
                            Console.WriteLine("Siusplau, digues un numero superior a 0 i menor o igual que " + tropas);
                        }

                    } catch (FormatException e)
                    {
                        Console.WriteLine("NOMÉS NUMEROS!!!");
                    }
                } while (tResposta <= 0 || tResposta > tropas);

                Region reg = null;
                foreach(Region r in SortedList)
                {
                    if(r.RegionID == rResposta)
                    {
                        reg = r;
                        break;
                    }
                }

                reg.Tropes = reg.Tropes + tResposta;
                tropas -= tResposta;

            } while (tropas > 0);

            Console.WriteLine();
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("Regions després de col·locar les tropes: ");
            foreach (Region r in SortedList) { Console.WriteLine("ID: " + r.RegionID + " | Nom: " + r.Nom + " | Tropes: " + r.Tropes); }
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------");
            Console.WriteLine();

            ctx.SaveChanges();

        }

        public static void start()
        {
            List<Continent> continents;
            List<Jugador> jugadors;
            List<Region> regions;

            continents = ctx.Continent.ToList();
            jugadors = ctx.Jugador.ToList();
            regions = ctx.Region.ToList();

            Random r = new Random();
            int ordre = r.Next(1, 2);
            jugadors[0].OrdreTirada = ordre;

            if (ordre == 1) jugadors[1].OrdreTirada = 2;
            else jugadors[1].OrdreTirada = 1;

            List<int> randomList = new List<int>();
            int region = 0;
            do
            {
                region = r.Next(1, 8);
                if (!randomList.Contains(region))
                {
                    randomList.Add(region);
                    regions[region].Jugador = jugadors[0];
                }
            } while (randomList.Count < 4);

            for (int i = 0; i < 8; i++)
            {
                if (!randomList.Contains(i))
                {
                    regions[i].Jugador = jugadors[1];
                }
            }

            int cont1 = 0;
            int cont2 = 0;

            foreach (Region reg in ctx.Region.ToArray())
            {
                if (reg.Continent.ContinentID == 1 && reg.Jugador == jugadors[0]) cont1++;
                if (reg.Continent.ContinentID == 2 && reg.Jugador == jugadors[0]) cont2++;
            }

            if (cont1 == 4) ctx.Continent.Where(c => c.ContinentID == 1).FirstOrDefault().Jugador = jugadors[0];
            else if (cont1 > 0) ctx.Continent.Where(c => c.ContinentID == 1).FirstOrDefault().Jugador = null;
            else ctx.Continent.Where(c => c.ContinentID == 1).FirstOrDefault().Jugador = jugadors[1];

            if (cont2 == 4) ctx.Continent.Where(c => c.ContinentID == 2).FirstOrDefault().Jugador = jugadors[0];
            else if (cont2 > 0) ctx.Continent.Where(c => c.ContinentID == 2).FirstOrDefault().Jugador = null;
            else ctx.Continent.Where(c => c.ContinentID == 2).FirstOrDefault().Jugador = jugadors[1];

            ctx.SaveChanges();
                
        }

        public static void init()
            {
                var region1 = new Region() { RegionID = 1, Nom = "Region1", Tropes = 4 };
                var region2 = new Region() { RegionID = 2, Nom = "Region2", Tropes = 4 };
                var region3 = new Region() { RegionID = 3, Nom = "Region3", Tropes = 4 };
                var region4 = new Region() { RegionID = 4, Nom = "Region4", Tropes = 4 };
                var region5 = new Region() { RegionID = 5, Nom = "Region5", Tropes = 4 };
                var region6 = new Region() { RegionID = 6, Nom = "Region6", Tropes = 4 };
                var region7 = new Region() { RegionID = 7, Nom = "Region7", Tropes = 4 };
                var region8 = new Region() { RegionID = 8, Nom = "Region8", Tropes = 4 };

                var jugador1 = new Jugador() { JugadorID = 1, Nom = "Joshua", QuantitatRegions = 4, NumVictories = 0 };
                var jugador2 = new Jugador() { JugadorID = 2, Nom = "Dani", QuantitatRegions = 4, NumVictories = 0 };

                var continent1 = new Continent() { ContinentID = 1, Nom = "Continent1", BonusTropes = 5 };
                var continent2 = new Continent() { ContinentID = 2, Nom = "Continent2", BonusTropes = 3 };

                //Regiones vecinas continente 1
                region1.Regions.Add(region2);
                region1.Regions.Add(region3);
                region1.Regions.Add(region4);
                region2.Regions.Add(region1);
                region3.Regions.Add(region1);
                region4.Regions.Add(region1);

                //Regiones vecinas continente 2
                region5.Regions.Add(region6);
                region5.Regions.Add(region8);
                region6.Regions.Add(region5);
                region6.Regions.Add(region8);
                region6.Regions.Add(region7);
                region7.Regions.Add(region6);
                region8.Regions.Add(region5);
                region8.Regions.Add(region6);

                //Regiones vecinas entre los dos continentes
                region1.Regions.Add(region5);
                region2.Regions.Add(region5);
                region4.Regions.Add(region5);
                region5.Regions.Add(region1);
                region5.Regions.Add(region2);
                region5.Regions.Add(region4);

                //Continente 1 Regiones 1-4
                region1.Continent = continent1;
                region2.Continent = continent1;
                region3.Continent = continent1;
                region4.Continent = continent1;

                //Continente 2 Regiones 5-8
                region5.Continent = continent2;
                region6.Continent = continent2;
                region7.Continent = continent2;
                region8.Continent = continent2;

                List<Region> regions = new List<Region>();
                regions.Add(region1);
                regions.Add(region2);
                regions.Add(region3);
                regions.Add(region4);
                regions.Add(region5);
                regions.Add(region6);
                regions.Add(region7);
                regions.Add(region8);

                List<Continent> continents = new List<Continent>();
                continents.Add(continent1);
                continents.Add(continent2);

                List<Jugador> jugadors = new List<Jugador>();
                jugadors.Add(jugador1);
                jugadors.Add(jugador2);

                ctx.Region.AddRange(regions);
                ctx.Continent.AddRange(continents);
                ctx.Jugador.AddRange(jugadors);

                ctx.SaveChanges();
            }

    }
}
