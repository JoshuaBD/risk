﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    public class RiskContext : DbContext
    {
        public RiskContext() : base("Risk") { }

        public DbSet<Region> Region { get; set; }
        public DbSet<Continent> Continent { get; set; }
        public DbSet<Jugador> Jugador { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<RiskContext>());
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RiskContext>());

            base.OnModelCreating(modelBuilder);
        }


    }
}
