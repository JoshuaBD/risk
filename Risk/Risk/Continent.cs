﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    public class Continent
    {
        [Key]
        public int ContinentID { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required, Column("bonus_tropes")]
        public int BonusTropes { get; set; }

        //Relació n a 1 amb Region
        public virtual ICollection<Region> Regions { get; set; }

        //Relació 1 a n amb Jugador
        public virtual Jugador Jugador { get; set; }

        public Continent()
        {
            this.Regions = new HashSet<Region>();
        }
    }
}
