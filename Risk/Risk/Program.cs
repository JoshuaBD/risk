﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    public class Program
    {
        static void Main(string[] args)
        {
            Services.init();
            Services.start();
            bienvenido();

            int ordre = 1;
            do
            {
                Services.turnBeggining(ordre);
                Services.attack(ordre);
                Services.reorganizar(ordre);
                Services.check(ordre);
                if(ordre == 2) ordre = 1;
                else ordre++;

            } while (ordre != 0);
        }

        static void bienvenido()
        {
            Console.WriteLine("==========================================================================");
            Console.WriteLine("=========================BENVINGUT AL JOC DE RISK=========================");
            Console.WriteLine("==========================================================================");

            using (var ctx = new RiskContext())
            {
                Console.WriteLine();
                Console.WriteLine("===========================SITUACIÓ INICIAL=============================");
                Console.WriteLine();

                List<Jugador> jugadors = ctx.Jugador.ToList();

                Console.WriteLine("Jugadors a la partida: ");
                Console.WriteLine();

                foreach (Jugador j in jugadors)
                {
                    Console.WriteLine("Jugador: " + j.Nom + " | Ordre Tirada: " + j.OrdreTirada);
                }

                List<Region> regions = ctx.Region.OrderBy(o => o.RegionID).ToList();

                Console.WriteLine();
                Console.WriteLine("Regions a la partida: ");
                Console.WriteLine();

                foreach (Region r in regions)
                {
                    String regionsVe = "";

                    foreach (Region rv in r.Regions2)
                    {
                        regionsVe += rv.RegionID + " ";
                    }

                    Console.WriteLine("ID: " + r.RegionID + " | Nom: " + r.Nom + " | RegionsVeines: " + regionsVe);
                }

                List<Continent> continents = ctx.Continent.ToList();

                Console.WriteLine();
                Console.WriteLine("Continents a la partida: ");
                Console.WriteLine();

                foreach (Continent c in continents)
                {
                    String regionsCont = "";
                    foreach (Region r in c.Regions)
                    {
                        regionsCont += r.RegionID + " ";
                    }
                    Console.WriteLine("Continent: " + c.Nom + " | Bonus Tropes: " + c.BonusTropes + " | Regions: " + regionsCont);
                }

                Console.WriteLine();
                Console.WriteLine("==========================================================================");
                Console.WriteLine("=============================COMENÇA EL JOC===============================");
                Console.WriteLine("==========================================================================");


            }
        }
    }
}
