﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Risk
{
    public class Region
    {
        public int RegionID { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required]
        public int Tropes { get; set; }
        //Relació 1 a n amb Jugadors
        public virtual Jugador Jugador { get; set; }

        //Relacio n a n amb Regions
        public virtual ICollection<Region> Regions { get; set; }
        public virtual ICollection<Region> Regions2 { get; set; }

        //Relació 1 a n amb Continent
        public virtual Continent Continent { get; set; }

        public Region()
        {
            this.Regions = new HashSet<Region>();
        }

    }
}
